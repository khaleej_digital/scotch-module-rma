#Beside_Rma Module

## Features

* FE customizations about RMA
* New column for Rma entity called return_point, customer is able to select this
* Admin user is able to see return point for created RMA
* Required fields has been updated
