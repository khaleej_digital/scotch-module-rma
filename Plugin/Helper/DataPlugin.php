<?php
/**
 * @category  Beside
 * @package   Beside_Rma
 * @author    Emrah Uyanik <emrah.uyanik@redboxdigital.com>
 * @copyright Copyright © 2021 Redbox Digital (http://www.redboxdigital.com)
 */

namespace Beside\Rma\Plugin\Helper;

use Magento\Sales\Api\OrderRepositoryInterface;

/**
 * Class DataPlugin
 * @package Beside\Rma\Plugin\Helper
 */
class DataPlugin
{
    const XML_PATH_DAY_LIMIT = 'beside_rma/general/day_limit';

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private $scopeConfig;
    /**
     * @var \Magento\Sales\Api\OrderRepositoryInterface
     */
    private $orderRepository;
    /**
     * @var \Magento\Rma\Model\ResourceModel\Item\CollectionFactory
     */
    private $rmaItemCollectionFactory;

    /**
     * DataPlugin constructor.
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Sales\Api\OrderRepositoryInterface $orderRepository
     * @param \Magento\Rma\Model\ResourceModel\Item\CollectionFactory $rmaItemCollectionFactory
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Sales\Api\OrderRepositoryInterface $orderRepository,
        \Magento\Rma\Model\ResourceModel\Item\CollectionFactory $rmaItemCollectionFactory
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->orderRepository = $orderRepository;
        $this->rmaItemCollectionFactory = $rmaItemCollectionFactory;
    }

    /**
     * Checks for ability to create RMA
     *
     * @param  int|\Magento\Sales\Model\Order $order
     * @param  bool $forceCreate - set yes when you don't need to check config setting (for admin side)
     * @return bool
     */
    public function aroundCanCreateRma(
        \Magento\Rma\Helper\Data $subject,
        callable $proceed,
        $order,
        $forceCreate = false
    ) {
        if(is_numeric($order)){
            $order = $this->orderRepository->get($order);
        }

        if (!$this->hasValidShipmentDate($order) || !$this->hasItemToReturn($order)) {
            return false;
        }

        return $proceed($order, $forceCreate);
    }

    /**
     * @param \Magento\Sales\Model\Order $order
     * @return bool
     */
    private function hasItemToReturn(\Magento\Sales\Model\Order $order)
    {
        $shippedItems = [];
        foreach ($order->getItemsCollection() as $item) {
            if (!(int)$item->getQtyShipped()) {
                continue;
            }
            $rmaItem = $this->rmaItemCollectionFactory->create()
                ->addFieldToFilter('order_item_id', $item->getId())
                ->getFirstItem();

            if ($item->getQtyShipped() > $rmaItem->getQtyRequested()) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param $order
     * @return bool
     * @throws \Exception
     */
    private function hasValidShipmentDate(\Magento\Sales\Model\Order $order)
    {
        if ($dateLimit = $this->scopeConfig->getValue(self::XML_PATH_DAY_LIMIT)) {
            $lastMonth = new \DateTime();
            $lastMonth->sub(new \DateInterval('P' . $dateLimit . 'D'));
            $shipment = $order->getShipmentsCollection()->getFirstItem();

            if ($lastMonth->format('Y-m-d H:i:s') > $shipment->getCreatedAt()) {
                return false;
            }
        }

        return true;
    }
}
