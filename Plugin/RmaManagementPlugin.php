<?php

namespace Beside\Rma\Plugin;

use Beside\Erp\Api\Data\ErpRequestInterface;
use Magento\Rma\Api\Data\RmaInterface;
use Magento\Rma\Api\RmaManagementInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\Exception\NoSuchEntityException;
use Beside\Erp\Api\UpdateReturnApiInterface;
use Beside\Erp\Api\CreateReturnApiInterface;
use Beside\Erp\Api\ErpRequestRepositoryInterface;
use Psr\Log\LoggerInterface;

class RmaManagementPlugin
{
    /**
     * @var CreateReturnApiInterface
     */
    private $createReturnApi;

    /**
     * @var UpdateReturnApiInterface
     */
    private $updateReturnApi;

    /**
     * @var ErpRequestRepositoryInterface
     */
    private $erpRequestRepository;

    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param CreateReturnApiInterface $createReturnApi
     * @param UpdateReturnApiInterface $updateReturnApi
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param ErpRequestRepositoryInterface $erpRequestRepository
     * @param LoggerInterface $logger
     */
    public function __construct(
        CreateReturnApiInterface $createReturnApi,
        UpdateReturnApiInterface $updateReturnApi,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        ErpRequestRepositoryInterface $erpRequestRepository,
        LoggerInterface $logger
    ) {
        $this->createReturnApi = $createReturnApi;
        $this->updateReturnApi = $updateReturnApi;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->erpRequestRepository = $erpRequestRepository;
        $this->logger = $logger;
    }

    /**
     * @param RmaManagementInterface $subject
     * @param RmaInterface|bool $rma
     * @return RmaInterface
     * @throws NoSuchEntityException
     */
    public function afterSaveRma(\Magento\Rma\Api\Data\RmaInterface $subject, $rma)
    {
        if ($rma && $rma instanceof RmaInterface) {
            $rmaId = $rma->getEntityId();

            try {
                if ($this->rmaCreated($rmaId)) {
                    $rmaData = $this->updateReturnApi->prepareData($rma);
                    $this->updateReturnApi->saveToQueue($rmaData, (int) $rma->getStoreId());
                } else {
                    $rmaData = $this->createReturnApi->prepareData($rma);
                    $this->createReturnApi->saveToQueue($rmaData, (int) $rma->getStoreId());
                }
            } catch (NoSuchEntityException $exception) {
                $this->logger->error($exception->getMessage());
            }
        }

        return $rma;
    }

    /**
     * @param $rmaId
     * @return bool
     */
    private function rmaCreated($rmaId)
    {
        $searchValue = strtr('%"rma_id":":rma_id_value"%', [':rma_id_value' => $rmaId]);
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter(ErpRequestInterface::API_TYPE, CreateReturnApiInterface::API_TYPE)
            ->addFilter(ErpRequestInterface::MESSAGE_TO_SEND, '%' . $searchValue . '%', 'like')
            ->create();

        return (bool) $this->erpRequestRepository->getList($searchCriteria)->getTotalCount();
    }
}
