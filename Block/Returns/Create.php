<?php
/**
 * @category  Beside
 * @package   Beside_Rma
 * @author    Emrah Uyanik <emrah.uyanik@redboxdigital.com>
 * @copyright Copyright © 2021 Redbox Digital (http://www.redboxdigital.com)
 */

namespace Beside\Rma\Block\Returns;

/**
 * Class Create
 * @package Beside\Rma\Block\Returns
 */
class Create extends \Magento\Rma\Block\Returns\Create
{
    /**
     * @var \Magento\Catalog\Helper\Image
     */
    private $imageHelper;

    /**
     * @var \Magento\Framework\UrlInterface
     */
    private $urlBuilder;

    /**
     * Create constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param \Magento\Framework\Data\Collection\ModelFactory $modelFactory
     * @param \Magento\Eav\Model\Form\Factory $formFactory
     * @param \Magento\Eav\Model\Config $eavConfig
     * @param \Magento\Rma\Model\ItemFactory $itemFactory
     * @param \Magento\Rma\Model\Item\FormFactory $itemFormFactory
     * @param \Magento\Rma\Helper\Data $rmaData
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Catalog\Helper\Image $imageHelper
     * @param \Magento\Framework\UrlInterface $urlBuilder
     * @param \Magento\Sales\Model\Order\Address\Renderer $addressRenderer
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Data\Collection\ModelFactory $modelFactory,
        \Magento\Eav\Model\Form\Factory $formFactory,
        \Magento\Eav\Model\Config $eavConfig,
        \Magento\Rma\Model\ItemFactory $itemFactory,
        \Magento\Rma\Model\Item\FormFactory $itemFormFactory,
        \Magento\Rma\Helper\Data $rmaData,
        \Magento\Framework\Registry $registry,
        \Magento\Catalog\Helper\Image $imageHelper,
        \Magento\Framework\UrlInterface $urlBuilder,
        \Magento\Sales\Model\Order\Address\Renderer $addressRenderer,
        \Magento\Rma\Helper\Eav $rmaEav,
        array $data = []
    ) {
        $this->urlBuilder = $urlBuilder;
        $this->imageHelper = $imageHelper;
        $this->rmaEav = $rmaEav;

        parent::__construct($context,
            $modelFactory,
            $formFactory,
            $eavConfig,
            $itemFactory,
            $itemFormFactory,
            $rmaData,
            $registry,
            $addressRenderer,
            $data
        );
    }

    /**
     * @param $_item
     * @return string
     */
    public function getItemImage($_item)
    {
        return $this->imageHelper->init($_item->getProduct(), 'small_image')
            ->setImageFile($_item->getProduct()->getSmallImage())->resize(200,200)
            ->getUrl();
    }

    /**
     * @return string
     */
    public function getSubmitUrl()
    {
        $order = $this->_coreRegistry->registry('current_order');

        return $this->getUrl('beside_rma/returns/submit',['order_id', $order->getId()]);
        return $this->urlBuilder->getUrl('beside_rma/returns/submit',

            ['_query' => ['order_id', $order->getId()]]
        );
    }

    /**
     * @param $_item
     * @return array|mixed
     */
    public function getProductOptions($_item)
    {
        if(!($productOptions = $_item->getProductOptions())){
            return [];
        }

        return isset($productOptions['attributes_info']) ? $productOptions['attributes_info'] : [];
    }

    /**
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function _construct()
    {
        $order = $this->_coreRegistry->registry('current_order');
        if (!$order) {
            return;
        }
        $this->setOrder($order);

        $items = $this->_rmaData->getOrderItems($order,true);
        $this->setItems($items);

        $formData = $this->_session->getRmaFormData(true);
        if (!empty($formData)) {
            $data = new \Magento\Framework\DataObject();
            $data->addData($formData);
            $this->setFormData($data);
        }
        $errorKeys = $this->_session->getRmaErrorKeys(true);
        if (!empty($errorKeys)) {
            $data = new \Magento\Framework\DataObject();
            $data->addData($errorKeys);
            $this->setErrorKeys($data);
        }
    }
    public function getReturnReasons(){
        return $this->rmaEav->getAttributeOptionValues('reason');
    }
}
