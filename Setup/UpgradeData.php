<?php
/**
 * @category  Beside
 * @package   Beside_Rma
 * @author    Emrah Uyanik <emrah.uyanik@redboxdigital.com>
 * @copyright Copyright (c) 2020 Redbox Digital (http://www.redboxdigital.com)
 */

namespace Beside\Rma\Setup;

use Magento\Eav\Model\Entity\Attribute\ScopedAttributeInterface;
use Magento\Eav\Model\Entity\Attribute\Source\Boolean;
use Magento\Eav\Setup\EavSetupFactory;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;

/**
 * Class UpgradeData
 * @package Beside\Rma\Setup
 */
class UpgradeData implements \Magento\Framework\Setup\UpgradeDataInterface
{
    /**
     * @var EavSetupFactory
     */
    private $eavSetupFactory;

    /**
     * UpgradeData constructor.
     * @param EavSetupFactory $eavSetupFactory
     */
    public function __construct(
        \Magento\Eav\Setup\EavSetupFactory $eavSetupFactory
    ) {
        $this->eavSetupFactory = $eavSetupFactory;
    }

    /**
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function upgrade(
        \Magento\Framework\Setup\ModuleDataSetupInterface $setup,
        \Magento\Framework\Setup\ModuleContextInterface $context
    ) {
        if (version_compare($context->getVersion(), '0.1.0', '<')) {
            $this->updateAttributes();
        }
    }

    /**
     * Update rma item attributes
     */
    public function updateAttributes()
    {
        $eavSetup = $this->eavSetupFactory->create();
        $eavSetup->updateAttribute(
            \Magento\Rma\Model\Item::ENTITY,
            'resolution',
            'is_required',
            Boolean::VALUE_NO
        );
        $eavSetup->updateAttribute(
            \Magento\Rma\Model\Item::ENTITY,
            'condition',
            'is_required',
            Boolean::VALUE_NO
        );
        $eavSetup->updateAttribute(
            \Magento\Rma\Model\Item::ENTITY,
            'reason',
            'is_required',
            Boolean::VALUE_NO
        );
        $eavSetup->updateAttribute(
            \Magento\Rma\Model\Item::ENTITY,
            'reason_other',
            'is_required',
            Boolean::VALUE_NO
        );
    }
}
